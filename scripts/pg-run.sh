output=$(docker inspect -f {{.State.Running}} my-pg)

# Checks if the container already exists
if [ $? -eq 0 ]; then
    # If it does
    if ($output); then
       echo PG is running madafaka!!
    else
        echo Starting the container...
        docker start my-pg
    fi
else
    echo Creating the image...
    # pg 10 on alpine
    # -d to detach
    # -v to mount to pgdata docker's volume (/var/lib/docker/volumes/pgdata/_data)
    docker run -d --name my-pg -e POSTGRES_USER=roee -e POSTGRES_PASSWORD=12345 -v pgdata:/var/lib/postgresql/data -p 5432:5432 postgres:10-alpine -d postgres
fi