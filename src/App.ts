import * as express from 'express'
import * as path from 'path'
import * as bodyParser from 'body-parser'
import * as helmet from 'helmet'
import * as compression from 'compression';
import * as logger from './utilities/logger'
import restApiRouter from './routes'

class App {
  public express: express.Application;
  
  constructor () {
    this.express = express();
    this.middlewares();
    this.routes();
  }

  private middlewares(): void {
    // TODO : activate the logger
    this.express.use(compression());
    this.express.use(helmet());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  private routes(): void {
    this.express.use('/', this.logApiCalls)
    this.express.use('/api/rest', restApiRouter);
  }

  private logApiCalls(req: express.Request, res:express.Response, next: express.NextFunction): void {
    logger.info(req.method ,
              " request to ", req.originalUrl, " from ", req.ip , 
              ", params: ", JSON.stringify(req.params), 
              ", body: ", JSON.stringify(req.body));
    next();
  }
}

export default new App().express