import app from './App'
import * as winston from 'winston'

const port = process.env.PORT || 3000

app.listen(port, (err) => {
  if (err) {
    winston.error(`Error while starting the server`, err);
  }

  winston.info(`server is listening on ${port}`);
});