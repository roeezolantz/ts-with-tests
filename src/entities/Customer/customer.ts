export default class Custumer {
    
    readonly id: number;
    readonly age: Date;
    first_name: string;
    last_name: string;
    nickname: string;

    constructor(id:number, age:Date, first_name:string, last_name:string, nickname:string) {
    }

    bark():string {
        console.log(`$(id) just barked`);
        return `Hi there, my name is $(this.first_name) $(this.last_name) but you can call me $(nickname)`;
    }
}