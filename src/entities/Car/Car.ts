import { Vehicle, IVehicleProps } from '../Vehicle';

export interface ICarProps extends IVehicleProps {
    numberOfDoors: number,
    steeringWheelSide: string
}

export class Car extends Vehicle implements ICarProps, Iterable<Map<string, string>> {
    
    public static TABLE_NAME = "tests.cars";
    private _numberOfDoors: number;
    private _steeringWheelSide: string;

    // constructor(options) {
    //     super(!options? '' : options.id, options.color, options,model, options.year, options.licencePlate);
    //     Object.assign(this, params);
    // }

    constructor(id?: number, color?: string, model?: string, year?: number, licencePlate?:string, numberOfDoors?:number, steeringWheelSide?: string) {
        super({id, color, model, year, licencePlate});
        this._numberOfDoors = numberOfDoors;
        this._steeringWheelSide = steeringWheelSide;
    }

    [Symbol.iterator] = function* () {
        
        for(let currKey of Object.keys(this)) {
            const keyWithoutUnderscore:string = currKey.substring(1);
            yield {}[keyWithoutUnderscore] = this[currKey];
        }
    }
    
    bark(name:string = "-_-"):string {
        return `Hi there, my name is $(this.model)`;
    }

	public get numberOfDoors(): number {
		return this._numberOfDoors;
	}

	public set numberOfDoors(value: number) {
		this._numberOfDoors = value;
	}

	public get steeringWheelSide(): string {
		return this._steeringWheelSide;
	}

	public set steeringWheelSide(value: string) {
		this._steeringWheelSide = value;
    }
}