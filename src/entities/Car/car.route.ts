import { cacheFile } from 'ts-jest/dist/utils';
import { WSAEUSERS } from 'constants';
import { Router, Request, Response } from 'express';
import * as CarController from '../../controllers/CarsContoller';
import { Car } from './Car'
import * as logger from '../../utilities/logger';

const router: Router = Router();
//const controller: ICarController = new CarController();

router.get('/', async (req: Request, res: Response) => {
  const allCars: Car[] = await CarController.getAllCars();
  res.json(allCars);
})

router.get('/:id', async (req: Request, res: Response) => {
  const { id } = req.params;
  const car: Car = await CarController.getCarById(id);
  res.json(car);
})

router.post('/', async (req: Request, res: Response) => {
  // Eject the params from the body by the car's properties types
  const {id, model, year, color, licencePlate, numberOfDoors, steeringWheelSide } = req.body;

  try {
    // Waits for the creation status
    let newCarId: number = await CarController.createNewCar(new Car(id, model, year, color, licencePlate, numberOfDoors, steeringWheelSide));
    res.json({id:newCarId});
  } catch(err) {
    res.status(500);
  }
})

export const CarsRoute: Router = router;