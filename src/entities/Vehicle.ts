export interface IVehicleProps {
    id: number,
    color: string,
    model: string,
    year: number,
    licencePlate: string
}

/**
 * This is a factory to generate childs of vehicle like cars and truks in a garage
 */
export interface VehicleConstructor {
    new (): Vehicle;
}

export abstract class Vehicle implements IVehicleProps {
    public static TABLE_NAME;
    protected _id: number;
    protected _color: string;
    protected _model: string;
    protected _year: number;
    protected _licencePlate: string;

    constructor(params: IVehicleProps) {
        Object.assign(this, params);
    }

    // constructor(_id?: number, _color?: string, _model?: string, _year?: number, _licencePlate?:string) {
    //     //Object.assign(this, params);
    // }

    public get id(): number {
        return this._id;
    }
    
    public set id(value: number) {
        this._id = value;
    }

    public get licencePlate(): string {
        return this._licencePlate;
    }

    public set licencePlate(value: string) {
        this._licencePlate = value;
    }

	public get color(): string {
		return this._color;
	}

	public set color(value: string) {
		this._color = value;
	}

	public get model(): string {
		return this._model;
	}

	public set model(value: string) {
		this._model = value;
	}
    
	public get year(): number {
		return this._year;
	}

	public set year(value: number) {
		this._year = value;
	}    
}