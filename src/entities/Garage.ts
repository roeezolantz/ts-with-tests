import { VehicleConstructor, Vehicle } from './Vehicle';
class Garage {
    private vehicles: Vehicle[];

    constructor() {
    }

    public addVehicleToGarage(newVehicle: VehicleConstructor) {
        this.vehicles.push(new newVehicle());
    }
}