import * as winston from 'winston';
import * as _ from 'lodash';
import * as fs from 'fs';
import * as path from 'path';

const pathify = (home:string, newP:string) : string => path.resolve(home, newP);

const logDir = pathify(__dirname, 'logs');
const logFiles = {
  error: pathify(logDir, 'error-logs.log')
}

// if (!fs.existsSync(logDir)) {
//   fs.mkdirSync(logDir);
// }

// Object.keys(logFiles).forEach(key => {
//   if (!fs.existsSync(logDir[key]))
//     fs.writeFileSync(logDir[key], '');
// })

const localeTime = function() :string {
  return (new Date()).toLocaleTimeString();
}

const colorizedText = function(options, text:string): string {
   return options.colorize ? winston.config.colorize(options.level, text) : text;
}

const logFormatter = (options:winston.LoggerOptions):string => {
  
  // Exports the meta if it is exists
  const meta = (options.meta && Object.keys(options.meta).length ? options.meta : null);
  
  // Expots the type of the log (DB/API etc..) from the meta if exists
  // .includes comes from es2017, it just looks like an error, but its cool
  const type: string = (meta && _.includes(Object.keys(meta), 'type') ? '(' + meta.type +')' : '');

  const essentialStackTrace = function(stack: string): string {
    return (stack ? stack.split('\n').slice(0,2).join('\n'): null);
  }

  return options.timestamp() + ' - ' + 
         colorizedText(options, options.level.toUpperCase()) + 
         type + ' - ' +
         (options.message ? options.message : '') +
         (meta && meta.stack ? '\nError stack : ' + essentialStackTrace(meta.stack) : '') +
         (meta && options.level == "error" ? '\nError : ' + JSON.stringify(meta.err) : '');
}

const logger = new winston.Logger({
  transports: [
    new (winston.transports.Console)({
      timestamp: localeTime,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      colorize: true,
      level: 'info',
      formatter: logFormatter
    }),
    new (winston.transports.File)({
      timestamp: localeTime,
      name: 'error-file',
      filename: logFiles.error,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      colorize: true,
      level: 'error'
    })
  ]
});

export = logger;