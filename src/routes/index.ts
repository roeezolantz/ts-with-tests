// ./routes/index.js

import { Router, NextFunction, Request, Response } from "express";

// import sub-routers
import { CarsRoute } from "./../entities/car/car.route"
import { CustomersRoute } from "./../entities/customer/customer.route";

const router: Router = Router();

// Mounts every router to its specific starting path
router.use('/car', CarsRoute);
router.use('/customer', CustomersRoute);

// Export the router
export default router;