import * as logger from '../utilities/logger';
import * as pg from 'pg';

const pool = new pg.Pool({
  user: 'roee',
  host: 'localhost',
  database: 'postgres',
  password: '12345',
  port: 5432,
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000
})

pool.on('connect', (client: pg.PoolClient) => {
  logger.info(`one has connected to the pool, total clients : ${pool.totalCount}`, {type:"DB"});
})

pool.on('remove', (client: pg.PoolClient) => {
  logger.info(`one has disconnected from the pool, total clients : ${pool.totalCount}`, {type:"DB"});
})

pool.on('error', (err: Error, client: pg.PoolClient) => {
  logger.error(`There was an error somewhere in pg.pool`, {err, type:"DB"});
})

export async function query(text: string, 
                            params? : any) : Promise<pg.QueryResult> {
                            // callback?:(err: Error ,result: pg.QueryResult)=>void) : Promise<pg.Query> {
                              console.log(params);
  return pool.query(text, params);
}