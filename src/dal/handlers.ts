import * as _ from 'lodash'

export interface IQueryKeysValues {
    keys: string[],
    values: string[]
  }
  
export const removeAttributeBeforeQueryByKey = (queryKeys:string[], attributeName:string, queryValues:string[]): IQueryKeysValues => {
    let keys = _.clone(queryKeys);
    let values = _.clone(queryValues);

    const attributeLocation = queryKeys.indexOf(attributeName);

    if (attributeLocation != -1) {
        values.splice(attributeLocation, 1);
    }

    // Gets all the wanted keys excludes _id
    keys = keys.filter(curr => curr != attributeName);
    return {keys, values};
};
  
export const generateDollarsPerKey = (keys: string[]) : string => {
    return _.times(keys.length, (num => '$' + _.add(num, 1))).join(',');
};
  
export const buildInsertQuery = (tableName:string, keys: string[]) : string => {
    return `INSERT INTO ${tableName}(${keys}) VALUES(${generateDollarsPerKey(keys)})`;
};