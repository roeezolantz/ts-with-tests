import * as logger from '../../utilities/logger';
import { Car } from '../../entities/car/car';
import { query } from '../index';
import * as _ from 'lodash'

export async function getAllCars() : Promise<Car[]>  {

  try {
    const { rows } = await query(`SELECT * FROM ${Car.TABLE_NAME};`);
    const cars: Car[] = rows.map(curr => new Car({...curr}));
    return cars;
  } catch(err) {
    logger.error("There was an error in getAllCars", {err, stack:err.stack, type:"DAL"}); 
  }
};

export async function getCarById(id: number) : Promise<Car> {
  try {
    const { rows } = await query(`SELECT * FROM ${Car.TABLE_NAME} WHERE id = $1`, [id])
    if (_.isEmpty(rows))
      return new Car(...rows[0]);
    else
      return null​​;
  } catch(err) {
    logger.error("There was an error in getCarById", {err, stack:err.stack, type:"DAL"});
  }
};