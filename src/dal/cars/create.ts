import * as pg from 'pg'
import { Car } from './../../entities/car/car'
import { query } from '../index';
import * as logger from '../../utilities/logger'
import { IQueryKeysValues, removeAttributeBeforeQueryByKey, buildInsertQuery } from '../handlers'

export async function createNewCar(car: Car, autoGenerateId: boolean = true) : Promise<number> {
  let values = [...car];
  let keys = Object.keys(car);
  
  if (autoGenerateId) {
    // Removes the _id from both the keys and the values
    ({keys, values} = removeAttributeBeforeQueryByKey(keys, '_id', values));
  }
  
  // Removes the _ from the keys
  keys = keys.map(curr => curr.slice(1));
  
  const queryText = buildInsertQuery(Car.TABLE_NAME, keys);
  
  // ------------------------------------------------------------------
  // Aditional way to build the query by specifying each param :
  // ------------------------------------------------------------------
  // const queryVars: pg.QueryConfig = {
  //   text: `INSERT INTO ${Car.TABLE_NAME}(model, year, licencePlate, color, numberOfDoors, steeringWheelSide) values($1, $2, $3, $4, $5, $6)`,
  //   values: [car.model, car.year, car.licencePlate, car.color, car.numberOfDoors, car.steeringWheelSide]
  // } 
  // ------------------------------------------------------------------

  try {
    return (await query(queryText, values)).rows[0];
  } catch(err) {
    logger.error("There was an error while inserting new car to the db", {err, stack:err.stack});
    // TODO : Add a good result with error handling
  }
}