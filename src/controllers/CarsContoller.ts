import { Router, Request, Response } from 'express';
import * as logger from '../utilities/logger'
import * as carsAccessor from '../dal/cars/index';
import { Car } from '../entities/car/car';

export async function getAllCars() : Promise<Car[]> {
    return carsAccessor.getAllCars();
}

export async function getCarById(id: number) : Promise<Car> {
    return carsAccessor.getCarById(id);
}

export async function createNewCar(car : Car) : Promise<number> {
    return carsAccessor.createNewCar(car);
}