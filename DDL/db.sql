DROP SCHEMA IF EXISTS tests CASCADE;
CREATE SCHEMA tests;

DROP TABLE IF EXISTS tests.vehicles CASCADE;

CREATE TABLE tests.vehicles (
  ID SERIAL PRIMARY KEY,
  licencePlate VARCHAR UNIQUE,
  model VARCHAR,
  year INTEGER,
  color VARCHAR
);

DROP TABLE IF EXISTS tests.cars CASCADE;

CREATE TABLE tests.cars (
  numberOfDoors INTEGER,
  steeringWheelSide VARCHAR
) INHERITS (tests.vehicles);